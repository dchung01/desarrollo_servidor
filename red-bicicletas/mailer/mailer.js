var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
    // all emails are catched by ethereal.email
    mailConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        user:process.env.ethereal_user,
        pass:process.env.ethereal_pwd
      }
    };

/*
let mailConfig;
    // all emails are catched by ethereal.email
    mailConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        user:process.env.ethereal_user,
        pass:process.env.ethereal_pwd
      }
    };
if (process.env.NODE_ENV === 'production') {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
} else {
    if (process.env.NODE_ENV === 'staging') {
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailConfig = sgTransport(options);
    } else {
        var sendMail = function(message, cb) {
            transporter.sendMail(message, (err, info) => {
                if (err) {
                    console.log('Error occurred. ' + err.message);
                    return process.exit(1);
                }
            });
        }
    }
}
*/
module.exports = nodemailer.createTransport(mailConfig);
